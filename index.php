<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/LogoNova/SystemHouse25.ico" type="image/x-icon">

    <title>System House</title>

    <!-- Bootstrap core CSS 
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <!-- Custom fonts for this template -->
    <link rel="stylesheet" href="font/font-awesome.min.css">
    <link rel="stylesheet" href="font/simple-line-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/estilos7.css" rel="stylesheet">
    <link href="css/card.css" rel="stylesheet">

  </head>

  <body id="page-top">


  	<!--STYLE PADDING PARA FORCAR A REMOCAO DO ESPACAMENTO NO CONTAINER FLUIDO-->
	  <div class="container-fluid" style="padding-left: 0px; padding-right: 0px;">
	
		<?php 
			/*###### BANCO #############*/
			include("utils/conexao.php");
			include("utils/banco.php");

			/*###### E-MAIL #############*/
			include("phpmailer/PHPMailer.php");
      include("phpmailer/SMTP.php");
    ?>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      
      <div class="container js-scroll-trigger">
        <a href="#"><img src="img/LogoNova/SystemHouse100.png" alt="System House Logo"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
          </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Quem Somos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#produtos">Produtos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#servicos">Servicos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contato</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- ################################ CAROUSEL ############################################# -->
    <header class="masthead">
      <?php include("includes/carousel.php"); ?>
    </header>

    <!-- ################################ ABOUT ############################################# -->
    <section class="about bg-primary text-center" id="about">
		  <div class="container">
			  <h2 class="display-5">Quem somos?</h2>
			  <p class="lead">
				<?php echo $descricao; ?><br />
			  </p>
			  <div class="col-md-12">
				  <div style="max-width: 500px; margin: auto">
			  		<img class="card-img-top" src="img/quemsomos1.jpg" alt="Consultoria em Tecnologia">
				  </div>
			  </div>	
        <hr class="my-4">
          <p><strong>MISSÃO: </strong> <span class="text-center"><?php echo $missao; ?></span></p>
          <p><strong>VISÃO: </strong> <span class="text-center"><?php echo $visao; ?></span></p>
        <p class="lead">
        <a class="btn btn-primary btn-md" href="#contato" role="button">Mais informações</a>
        </p>
		  </div>
    </section>

    <!-- ################################ PRODUTOS ############################################# -->
    <section class="produtos bg-fixo text-center" id="produtos">
        <div class="container">
          <div class="row margin-menu">
            <div class="col-md-12">
              <h2 class="display-5">Conheça nossos produtos</h2>
              </div>
            </div>
          </div>
          </p>
          <div class="row">
            <div class="col-md-4">
              <div class="card card-member">
                <div class="card-header text-center">
                  OS - House
                </div>
                <div class="card-body">
                  <h4 class="card-title">Sistema de Ordem de Serviço</h4>
                  <p class="card-text text-justify">
                    Tenha o controle dos serviços da sua empresa com o <strong>OS - House</strong>, com ele é possível
                    criar, organizar e manter as ordens de serviços de uma maneira fácil e rápida e sem burocracias.
                  </p>
                  <a href="#" class="btn btn-primary">Saiba mais</a>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card card-member">
                <div class="card-header text-center">
                  ImobiExpress
                </div>
                <div class="card-body">
                  <h4 class="card-title">Sistema para Imobiliárias</h4>
                  <p class="card-text text-justify">
                    Gerencie os imóveis dos seus clientes com essa ferramenta que permite fazer upload de imagens,
                    disponibilizar um imóvel e alterar as informações de uma maneira muito fácil.
                  </p>
                  <a href="#" class="btn btn-primary">Saiba mais</a>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card card-member">
                <div class="card-header text-center">
                  SoccerApp
                </div>
                <div class="card-body">
                  <h4 class="card-title">Agendamento de Horários</h4>
                  <p class="card-text text-justify">
                    Agende o horário do futebol com os amigos, com apenas alguns cliques é possível agendar horários
                    para jogar aquele futebol com os amigos e sem precisar ficar ligando.
                  </p>
                  <a href="#" class="btn btn-primary">Saiba mais</a>
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>

    
    <!-- ################################ SERVIÇOS ############################################# -->
    <section class="servicos bg-fixo" id="servicos">
        <div class="container ">
          <div class="row">
            <div class="col-md-12">
              <h2 class="display-5 text-center">Conheça nossos serviços</h2>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card" style="width: 100%;">
                <img class="card-img-top" src="img/desenvolvimento.jpg" alt="Desenvolvimento de Sites">
                <div class="card-body">
                  <h4 class="card-title">Desenvolvimento de Sites</h4>
                  <p class="card-text">
                    Se você precisa de um site para mostrar sua empresa ou um produto, você está no
                    lugar certo.
                  </p>
                  <a href="#" class="btn btn-primary">Conheça</a>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="card" style="width: 100%;">
                <img class="card-img-top" src="img/consultoria.jpg" alt="Consultoria em Tecnologia">
                <div class="card-body">
                  <h4 class="card-title">Consultoria em Tecnologia</h4>
                  <p class="card-text">
                    Sua empresa precisa de um conhecimento especializado em tecnologia? Entre em contato
                    com nossa equipe.
                  </p>
                  <a href="#" class="btn btn-primary">Conheça</a>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>

    <!-- ################################ CONTATOS ############################################# -->
    <section class="contact bg-fixo1" id="contact">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
              <div style="max-width: 250px; margin: auto">
                  <img class="card-img-top" src="img/contato.png" alt="Fale Conosco">
              </div>
              <h4 class="text-justify text-center text-primary">Fale Conosco</h4><hr>
          </div>

          <div class="col-md-8 card-header card-member">

            <?php
              if(!empty($_GET["status"])) {
                echo
                '<p class="alert alert-success efeito-fade">'.
                '  E-mail enviado com sucesso!'.
                '</p>';
              }
            ?>

            <h3 class="text-center text-primary ">Contato System House</h3><hr>
            <p class="text-center">Faça um orçamento, ou para dúvidas e esclarecimentos!!</p>

              <div class="form-group-sm">
                  <form method="post" action="utils/envioDeEmail.php" name="form_contato" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" id="idNome" placeholder="Nome" name="nome" required="">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="idEmail" placeholder="E-mail" name="email"
                              required>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" id="idTelefone" placeholder="Telefone" name="telefone"
                              required="">
                    </div>
                    <div class="form-group">
                            <input type="text" class="form-control" id="idAssunto" placeholder="Assunto" name="assunto"
                              required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="textarea_mensagem" placeholder="Digite aqui sua mensagem"
                                  name="mensagem" required=""></textarea>
                    </div>
                    <button type="reset" class="btn btn-primary">Limpar</button>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                  </form>
              </div>
          </div>
        </div>
      </div>
    </section>

    <!-- ################################ FOOTER ############################################# -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <h3 class="text-primary">System House</h3>
            <a class="navbar-brand" href="index.php"><img src="img/LogoNova/SystemHouse180.png" alt="System House Logo"></a>

            <p class="footer-links">
              <a href="#">Home</a>
            </p>
            <p class="footer-empresa">System House &copy; 2017 - 2018</p>
           </div>

          <div class="col-md-4">
      		  <div>
			        <i class="fa fa-map-marker"></i>
			        <p><span><?php echo $logradouro ?></span> Minas Gerais, Araguari</p>
		        </div>
		        <div>
              <i class="fa fa-phone"></i>
                <p><span>
                  <?php echo $celular_1; ?></br>
                  <?php echo $celular_2; ?></br>
                </p></span>
            </div>
            <div>
              <i class="fa fa-envelope"></i>
              <p><a href="mailto:suporte@systemhouse.com.br">
                <?php echo $email; ?>
              </a></p>
            </div>       
          </div>


          <div class="col-md-4">
            <p class="footer-sobre">
              <h4><span class="text-primary">Sobre a empresa</span></br></h4>
              System House é uma empresa que oferece produtos e serviços de tecnologia para sua empresa.
              Conheça a System House, entre em contato através do link de contato.
            </p>
            <div class="footer-icones">   
              <p><a href="https://www.facebook.com/SystemHouseFace/"><i class="fa fa-facebook"></i>   Facebook</a></p>
              <p><a href="#"><i class="fa fa-twitter"></i> Twitter</a></p>
              <p><a href="#"><i class="fa fa-youtube"></i> YouTube</a></p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  

    <!-- Bootstrap core JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/new-age.min.js"></script>

  </body>

</html>
