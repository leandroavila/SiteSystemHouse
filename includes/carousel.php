        
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img class="d-block w-100" src="img/Slide01.jpg" alt="First slide">
					<div class="carousel-caption d-none d-md-block text-left">
            			<p><a class="btn btn-md btn-primary" href="#" role="button">Mais Informações</a></p>
          			</div>
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="img/Slide02.jpg" alt="Second slide">
					<div class="carousel-caption d-none d-md-block">
            			<p><h4>Consultoria em Tecnologia</h4></p>
            			<p><a class="btn btn-md btn-warning" href="#" role="button">Mais Informações</a></p>
          			</div>
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="img/Slide03.jpg" alt="Third slide">
					<div class="carousel-caption d-none d-md-block text-left">
            			<p><h5>Desenvolvimento Android</h5></p>
            			<p><a class="btn btn-md btn-primary" href="#" role="button">Mais Informações</a></p>
          			</div>
				</div>
			</div>
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>