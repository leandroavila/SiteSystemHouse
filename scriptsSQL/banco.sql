--Para criar Bando Dados
CREATE DATABASE db_cadastro
DEFAULT CHARACTER SET  utf8
DEFAULT COLLATE utf8_general_ci;
 
 CREATE TABLE `pessoas`( 
   `id` int NOT NULL AUTO_INCREMENT, 
   `nome` varchar(30) NOT NULL, 
   `nascimento` date, 
   `sexo` enum('M', 'F'), 
   `peso` decimal(5,2), 
   `altura` decimal(3,2), 
   `nacionalidade` varchar(20) DEFAULT 'Brasil', 
   PRIMARY KEY (`id`) 
 )DEFAULT CHARSET = utf8
 
--UNIQUE para dizer que este campo não pode receber outro igual
--UNSIGNED para que esta não seja negativo
CREATE TABLE IF NOT EXISTS `cursos` (
    nome varchar(30) NOT NULL UNIQUE,
    descrição text,
    carga int UNSIGNED,
    totaulas int UNSIGNED,
    ano year DEFAULT '2018'
)DEFAULT CHARSET = utf8;

alter TABLE `cursos`
add column idcurso int first;

alter TABLE `cursos`
add PRIMARY KEY (idcurso);



 INSERT INTO `pessoas` 
    (`id`, `nome`, `nascimento`, `sexo`, `peso`, `altura`, `nacionalidade`)
    VALUES 
    (DEFAULT, 'Wellington', '1990-06-25', 'M', 90.5, 1.78, 'EUA'),
    (DEFAULT, 'Madalena', '1980-08-15', 'F', 68.5, 1.65, DEFAULT),
    (DEFAULT, 'Paula', '1995-06-10', 'F', 55.5, 1.60, DEFAULT);

 INSERT INTO `cursos`
    (`idcurso`, `nome`, `descricao`, `carga`, `totaulas`, `ano`)
 VALUES
    ('1','HTML4', 'Curso de HTML5','40','37','2014'),
    ('2','Algoritmos', 'Lógica de Programação','20','15','2014'),
    ('3','Photoshop', 'Dicas de Photoshop CC','10','8','2014'),
    ('4','PGP', 'Curso de PHP para iniciantes','40','20','2010'),
    ('5','Jarva', 'Introdução à Linguagem Java','10','29','2000'),
    ('6','MySQL', 'Banco de Dados MySQL','30','15','2016'),
    ('7','Word', 'Curso completo de Word','40','30','2018'),
    ('8','Sapateado', 'Danças Rítmicas','40','30','2018'),
    ('9','Cozinha Árabe', 'Danças Rítmicas','40','30','2018'),
    ('10','YouTuber', 'Gerar polêmicas e ganhar inscritos','5','2','2018'); 
 

--Selecionar tudo de uma tabela
select * from `pessoas`;

--Listar em ordem descrescente
 desc `pessoas`;

 --Adicionar um coluna (campo) na tabela
 alter table `pessoas`
 add column `profissao` varchar(10) after `nome`;

--Deletar ou remover uma coluna (campo) na tabela
 alter table `pessoas`
 drop column `profissao`;

--Alterar a descrição da coluna
 alter table `pessoas`
 modify column `profissao` varchar(20);

--Update do conteuda da tabela
 UPDATE `cursos` 
 SET `nome` = 'Java', `carga` = '40', `ano` =  '2015' 
 WHERE `idcurso` = 5;