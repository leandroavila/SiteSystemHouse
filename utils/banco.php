<?php

    $conexao = getConnection();
    $preparedStatement = $conexao->prepare('SELECT DESC_EMPRESA, EMAIL,  CELULAR_1, CELULAR_2, LOGRADOURO, NRO_LOGRADOURO, BAIRRO, CIDADE, ESTADO, PAIS, MISSAO, VISAO, VALORES FROM TB_EMPRESA WHERE ID = :idEmpresa');
    $idEmpresa = 1;

    $preparedStatement->bindParam(':idEmpresa', $idEmpresa);
    $preparedStatement->execute();

    $resultado = $preparedStatement->fetch();

    $descricao = $resultado["DESC_EMPRESA"];
    $email =  $resultado["EMAIL"]; 
    $celular_1 = $resultado["CELULAR_1"];
    $celular_2 = $resultado["CELULAR_2"];
    $logradouro = $resultado["LOGRADOURO"];
    $nro_logradouro = $resultado["NRO_LOGRADOURO"];
    $bairoo = $resultado["BAIRRO"];
    $cidade = $resultado["CIDADE"];
    $estado = $resultado["ESTADO"];
    $pais = $resultado["PAIS"];
    $missao = $resultado["MISSAO"];
    $visao = $resultado["VISAO"];
    $valores = $resultado["VALORES"];
?>