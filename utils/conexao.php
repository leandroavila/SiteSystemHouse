<?php
  //CRIANDO A CONEXAO COM PDO MYSQL

  //TESTA SE ESTE RECURSO FOI SOLICITADO POR URL
  //SE [TRUE] REDIRECIONAR PARA A PAGINA CORRETA DE TESTE - conexao.php
  if($_SERVER['REQUEST_URI'] == "/site-principal/utils/conexao.php") {
    header("location: /site-principal/utils/testeConexao.php");
  }

  //FUNCAO QUE RETORNA A CONEXAO
  function getConnection() {
    //BLOCO TRY PARA CAPTURA DE ERROS
    try {
      //VARIAVEIS DE CONEXAO
      $host = "localhost";
      $banco = "db_site";
      $user = "root";
      $pass = "";
      $charset = "utf8";

      //OBTENDO A CONEXAO
      $con = new PDO("mysql:host=".$host.";dbname=".$banco.";charset=".$charset, $user, $pass);

      //SETANDO O MODO DE ERRO PARA LANCAR EXCECOES
      $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      return $con;

    } catch (PDOException $exception) {
        //QUALQUER PROBLEMA QUE ACONTECER COM A CONEXAO, O FLUXO IRA CAIR AQUI E O ERRO SERA MOSTRADO
        print "ERRO!: " . $exception->getMessage() . "<br/>";
        //PARA A EXECUCAO DO SISTEMA
        die();
    }

  }

?>
